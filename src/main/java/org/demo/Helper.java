package org.demo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.camel.Exchange;
import org.apache.cxf.binding.soap.SoapHeader;
import org.apache.cxf.headers.Header;
import org.w3c.dom.NodeList;

public class Helper {
	
	//Fetches CXF Soap Headers and prepares them as input for XSLT
    public void prepareXsltSoapHeaders(Exchange exchange)
    {
    	//fetch CXF SOAP headers
        List<Header> list = (List<Header>) exchange.getIn().getHeader("org.apache.cxf.headers.Header.list");

        //generate new list with XML Elements
        ArrayList elements = new ArrayList();
	    for (Header header : list) {
	        elements.add(header.getObject());
	    }

	    //set list in a header (XSLT input)
        exchange.getIn().setHeader("soapHeaders",elements);
    }

    //Fetches Soap Headers from XSLT execution and prepares CXF headers
    public void prepareCxfSoapHeaders(Exchange exchange)
    {
    	//Required qualified name
        QName qname = QName.valueOf("{http://apache.org/camel/component/cxf/soap/headers}SOAPHeaderInfo");

        //obtain XSLT output Soap Headers
        NodeList list = exchange.getProperty("xslt-soap-headers", NodeList.class);
        
        //generate new List of SoapHeaders in CXF expected format
        List<SoapHeader> headers = new ArrayList<>();
        for(int i=0; i < list.getLength(); i++){
            headers.add(new SoapHeader(qname, list.item(i)));
        }

        //set header CXF expects with Soap Headers
        exchange.getIn().setHeader("org.apache.cxf.headers.Header.list", headers);
    }
    
}
